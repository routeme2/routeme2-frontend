import Vue from 'vue'
import Vue2Filters from 'vue2-filters'
import { sync } from 'vuex-router-sync'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import 'roboto-fontface/css/roboto/roboto-fontface.css'
import '@mdi/font/css/materialdesignicons.css'
import router from './router'
import store from './store'


Vue.use(Vue2Filters)
Vue.filter('capitalizeAll', s => (s ? s.replace(/\b\w/g, l => l.toUpperCase()) : ''))
sync(store, router)
new Vue({
  vuetify,
  router,
  store,
  render: h => h(App),
  
  created() {
    if (process.env.NODE_ENV === 'development') {
      console.log('Created')
    } else {
      Vue.config.productionTip = false
    }
  }
}).$mount('#app')
