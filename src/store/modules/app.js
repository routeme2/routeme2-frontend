import { routeData } from '@/router'

export const state = {
  BarHeight: 60,
  RouteData: routeData,
  SignInModal: false
}

export const getters = {}

export const mutations = {
  // Only add custom mut here, basic mut is done.
}

for (let s of Object.keys(state)) {
  mutations[`mut${s}`] = (state, payload) => { state[s] = payload }
  if (typeof state[s] === 'boolean') mutations[`mutToggle${s}`] = (state) => { state[s] = !state[s] }
}

export const actions = {}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
