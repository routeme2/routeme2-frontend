import vuetify from '@/plugins/vuetify'
import filters from '@/plugins/filters'
import vueRouterSync from '@/plugins/vueRouterSync'
import '@mdi/font/css/materialdesignicons.css'

const registerPlugins = () => {
  vuetify()
  filters()
  vueRouterSync()
}

export default registerPlugins
