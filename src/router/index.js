import Vue from 'vue'
import VueRouter from 'vue-router'
import TemplatePage from '../views/TemplatePage.vue'
import Welcome from '../views/Welcome.vue'

Vue.use(VueRouter)

export const routeData = [{
  meta: {
    icon: 'mdi-home'
  },
  name: 'home',
  path: '/',
  component: Welcome
}, {
  meta: {
    icon: 'mdi-star'
  },
  name: 'List Route',
  path: '/list_route',
  component: TemplatePage,
  children: [{
    meta: {
      icon: 'mdi-history'
    },
    name: 'Sub Route 1',
    path: 'sub_route_1',
    component: TemplatePage
  }, {
    meta: {},
    name: 'Sub Route 2',
    path: 'sub_route_2',
    component: TemplatePage
  }]
}]

const router = new VueRouter({
  routes: routeData
})

export default router
